package com.n.bachelor;

/**
 * Class for constructing an object from user data.
 */
public class User implements Comparable<User> {

    private String name, roomcode;
    private long score;
    private boolean host;

    /**
     * Constructor for creating user object when only the name is given
     * @param name - name of the user
     */
    public User(String name) {
        this.name = name;
        this.score = -1;
    }

    /**
     * Constructor for creating the user object.
     * @param name - name of the user
     * @param roomcode - what room the user is in
     * @param score - the users score
     * @param host - if the user is host or not
     */
    public User(String name, String roomcode, long score, boolean host){
        this.name = name;
        this.roomcode = roomcode;
        this.score = score;
        this.host =  host;
    }


    /**
     * Get and set methods.
     * Retrieve or set different values of a object
     */

    public String getName(){
        return this.name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getRoomcode(){
        return this.roomcode;
    }

    public void setRoomcode(String roomcode){
        this.roomcode = roomcode;
    }

    public long getScore(){
        return this.score;
    }

    public void setScore(long s){
        this.score = s;
    }

    public boolean getHost(){
        return this.host;
    }

    public void setHost(boolean h){
        this.host = h;
    }

    public int compareTo(User otherUser) {
        return Long.compare(otherUser.score, this.score);
    }

}
